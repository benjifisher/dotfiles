source $VIMRUNTIME/defaults.vim
packadd! matchit

set hls incsearch
set laststatus=2
set ruler sw=2 et tw=80
set shortmess-=S
set so=0
set tabpagemax=34
set wildmode=longest,full

nmap ,cd :lcd %:p:h<CR>

filetype plugin indent on

function! HtmlDecode() range
  execute a:firstline . "," . a:lastline . "s/&#39;/'/eg"
  execute a:firstline . "," . a:lastline . 's/&quot;/"/eg'
  execute a:firstline . "," . a:lastline . 's/[\u201c\u201d]/"/eg'
  execute a:firstline . "," . a:lastline . 's/&lt;/</eg'
  execute a:firstline . "," . a:lastline . 's/&gt;/>/eg'
  execute a:firstline . "," . a:lastline . 's/&amp;/\&/eg'
endfunction
command -range=% -bar HtmlDecode <line1>,<line2> call HtmlDecode()
command MarkdownToHtml execute "%!pandoc -t html" <Bar> %HtmlDecode <Bar> %y+

command! -nargs=1 Review colorscheme solarized <bar> tabdo vertical Gdiffsplit <args>

" Vdebug configuration
" :silent VdebugOpt port 9003
" :let g:vdebug_options.path_maps = {'/var/www/html': '/home/benji/Sites/drupal'}
" :let g:vdebug_options.watch_window_style = 'compact'

