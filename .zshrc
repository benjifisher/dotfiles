# Set architecture-specific brew share path.
# Based on https://github.com/geerlingguy/dotfiles/blob/master/.zshrc
# Todo: Test uname and set brew_path correctly for Linux.
arch_name="$(uname -m)"
if [ "${arch_name}" = "x86_64" ]; then
    brew_path="/usr/local"
elif [ "${arch_name}" = "arm64" ]; then
    brew_path="/opt/homebrew"
else
    echo "Unknown architecture: ${arch_name}"
fi

if type $brew_path/bin/brew &>/dev/null
then
  PATH="$($brew_path/bin/brew --prefix)/bin:${PATH}"
  FPATH="$($brew_path/bin/brew --prefix)/share/zsh/site-functions:${FPATH}"

  autoload -Uz compinit
  compinit
fi

# Add the Python path so that ansible is available.
# To do: it will not always be 3.9, will it?
PATH="${PATH}:$HOME/Library/Python/3.9/bin"

source ~/.aliases
